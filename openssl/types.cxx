// file      : openssl/types.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <openssl/types.hxx>

namespace openssl
{
  string
  to_string (simulate_outcome s)
  {
    switch (s)
    {
    case simulate_outcome::success: return "success";
    case simulate_outcome::failure: return "failure";
    }

    assert (false);
    return string ();
  }

  simulate_outcome
  to_simulate_outcome (const string& s)
  {
         if (s == "success") return simulate_outcome::success;
    else if (s == "failure") return simulate_outcome::failure;
    else throw invalid_argument ("invalid simulate outcome '" + s + '\'');
  }
}
