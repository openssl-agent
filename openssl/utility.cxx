// file      : openssl/utility.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <openssl/utility.hxx>

#include <strings.h> // explicit_bzero()

#include <cstdlib> // exit()
#include <cstring> // memset()

namespace openssl
{
  using namespace std;

  void
  mem_clear (void* p, size_t n)
  {
    // Note that explicit_bzero() is only available in glibc starting 2.25.
    //
#if defined (__GLIBC__)       && \
    defined (__GLIBC_MINOR__) && \
    (__GLIBC__ > 2 || __GLIBC__ == 2 && __GLIBC_MINOR__ >= 25)

    explicit_bzero (p, n);

#else

    memset (p, 0, n);

    // Make sure the function is not optimized out.
    //
    for (char *i (reinterpret_cast<char*> (p)), *e (i + n); i != e; ++i)
    {
      if (*i != '\0')
        exit (*i);
    }

#endif
  }
}
