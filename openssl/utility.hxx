// file      : openssl/utility.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef OPENSSL_UTILITY_HXX
#define OPENSSL_UTILITY_HXX

#include <string>  // to_string()
#include <utility> // move(), forward(), declval(), make_pair()
#include <cassert> // assert()

#include <libbutl/utility.hxx>    // icasecmp(), reverse_iterate(), etc
#include <libbutl/fdstream.hxx>
#include <libbutl/filesystem.hxx>

#include <openssl/types.hxx>
#include <openssl/version.hxx>

namespace openssl
{
  using std::move;
  using std::forward;
  using std::declval;

  using std::make_pair;
  using std::to_string;

  // <libbutl/utility.hxx>
  //
  using butl::ucase;
  using butl::lcase;
  using butl::icasecmp;

  using butl::trim;
  using butl::next_word;

  using butl::reverse_iterate;

  using butl::make_guard;
  using butl::make_exception_guard;

  using butl::getenv;
  using butl::setenv;
  using butl::unsetenv;

  using butl::eof;

  using butl::throw_generic_error;
  using butl::throw_system_error;

  using butl::throw_generic_ios_failure;
  using butl::throw_system_ios_failure;

  // <libbutl/filesystem.hxx>
  //
  using butl::file_exists;

  using butl::auto_rmfile;
  using butl::auto_rmdir;

  // <libbutl/fdstream.hxx>
  //
  using butl::stdin_fdmode;
  using butl::stdout_fdmode;
  using butl::fdopen_null;
  using butl::fddup;

  // Securely clear a memory buffer.
  //
  void
  mem_clear (void*, size_t);
}

#endif // OPENSSL_UTILITY_HXX
