// file      : openssl/diagnostics.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <openssl/diagnostics.hxx>

namespace openssl
{
  // Diagnostic facility, project specifics.
  //

  void simple_prologue_base::
  operator() (const diag_record& r) const
  {
    if (type_ != nullptr)
      r << type_;

    if (name_ != nullptr)
      r << name_;

    if (data_ != nullptr)
      r << '(' << data_ << ')';

    if (name_ != nullptr || data_ != nullptr)
      r << ": ";
  }

  basic_mark error ("error: ");
  basic_mark warn  ("warning: ");
  basic_mark info  ("info: ");
  basic_mark text  (nullptr);
  fail_mark  fail  ("error: ");

  const fail_end endf;
}
