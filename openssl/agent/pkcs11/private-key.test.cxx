// file      : openssl/agent/pkcs11/private-key.test.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <iostream>

#include <openssl/agent/pkcs11/url.hxx>
#include <openssl/agent/pkcs11/private-key.hxx>

#undef NDEBUG
#include <cassert>

// Usage: argv[0] <pkcs11-url>
//
// Create private_key object referenced by the <pkcs11-url>. Read data from
// stdin, sign it with the private key, and print the signature to stdout.
//
int
main (int argc, char* argv[])
{
  using namespace std;
  using namespace openssl;
  using namespace openssl::agent::pkcs11;

  if (argc != 2)
  {
    cerr << "usage: " << argv[0] << " <pkcs11-url>" << endl;
    return 1;
  }

  cin.exceptions  (ios::badbit);
  cout.exceptions (ios::failbit | ios::badbit);

  try
  {
    url                     u   (argv[1]);
    agent::pkcs11::identity idn (u);
    agent::pkcs11::access   acc (u);

    vector<char> data ((istreambuf_iterator<char> (cin)),
                       istreambuf_iterator<char> ());

    vector<char> signature;

    // Stress the API a bit recreating, reusing and having concurrent keys.
    //
    for (size_t i (0); i < 5; ++i)
    {
      private_key key1 (idn, acc, nullptr /* secure_pin */);
      private_key key2 (idn, acc, nullptr /* secure_pin */);

      for (size_t i (0); i < 10; ++i)
      {
        vector<char> sign ((i % 2 == 0 ? key1 : key2).sign (data));

        if (signature.empty ())
          signature = move (sign);
        else if (sign != signature)
          throw runtime_error ("sign operation is unreliable");
      }
    }

    cout.write (signature.data (), signature.size ());
    return 0;
  }
  catch (const invalid_argument& e)
  {
    cerr << e << endl;
    return 1;
  }
  catch (const runtime_error& e)
  {
    cerr << e << endl;
    return 1;
  }
}
