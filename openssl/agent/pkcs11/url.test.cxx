// file      : openssl/agent/pkcs11/url.test.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <iostream>

#include <openssl/agent/pkcs11/url.hxx>

#undef NDEBUG
#include <cassert>

// Usage: argv[0]
//
// Create pkcs11::url objects from stdin lines, and for each of them print its
// string representation to stdout, one per line.
//
int
main ()
{
  using namespace std;
  using namespace openssl;
  using namespace openssl::agent::pkcs11;

  cin.exceptions  (ios::badbit);
  cout.exceptions (ios::failbit | ios::badbit);

  try
  {
    string s;
    while (!eof (getline (cin, s)))
    {
      url u (s);

      // Validate the URL attributes.
      //
      agent::pkcs11::identity idn (u);
      agent::pkcs11::access   acc (u);

      cout << u << endl;
    }

    return 0;
  }
  catch (const exception& e)
  {
    cerr << e << endl;
    return 1;
  }
}
