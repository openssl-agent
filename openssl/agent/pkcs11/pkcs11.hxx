// file      : openssl/agent/pkcs11/pkcs11.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef OPENSSL_AGENT_PKCS11_PKCS11_HXX
#define OPENSSL_AGENT_PKCS11_PKCS11_HXX

// PKCS#11 API (Cryptoki) definitions.
//
#include <openssl/agent/pkcs11/pkcs11.h>

#include <openssl/types.hxx>
#include <openssl/utility.hxx>

namespace openssl
{
  namespace agent
  {
    namespace pkcs11
    {
      // For simplicity we will not handle multiple PKCS#11 modules
      // simultaneously. The first one loaded will stay till the end of the
      // process lifetime.
      //

      // Return the PKCS#11 API pointer. If requested, ignore non-existent
      // module returning NULL.
      //
      // On the first call load the PKCS#11 module using the specified path
      // and initialize the API. Return the same pointer on the subsequent
      // calls regardless of the path. Throw runtime_error if anything goes
      // wrong.
      //
      CK_FUNCTION_LIST*
      api (const path&, bool ignore_nonexistent = false);

      // Return a pointer to the previously initialized PKCS#11 API.
      //
      CK_FUNCTION_LIST*
      api ();

      // Throw runtime_error describing a PKCS#11 API error.
      //
      [[noreturn]] void
      throw_api_error (CK_RV error, string what);

      // Convert API string representation to a regular one.
      //
      // PKCS#11 API struct string members are fixed-sized unsigned character
      // arrays right-padded with the space character. Return such a string
      // with the trailing spaces stripped.
      //
      inline string
      api_string (const unsigned char* s, size_t n)
      {
        for (; n != 0 && s[n - 1] == ' '; --n) ;
        return string (reinterpret_cast<const char*> (s), n);
      }
    }
  }
}

#endif // OPENSSL_AGENT_PKCS11_PKCS11_HXX
