// file      : openssl/types-parsers.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

// CLI parsers, included into the generated source files.
//

#ifndef OPENSSL_TYPES_PARSERS_HXX
#define OPENSSL_TYPES_PARSERS_HXX

#include <openssl/types.hxx>

namespace openssl
{
  namespace cli
  {
    class scanner;

    template <typename T>
    struct parser;

    template <>
    struct parser<simulate_outcome>
    {
      static void
      parse (simulate_outcome&, bool&, scanner&);
    };
  }
}

#endif // OPENSSL_TYPES_PARSERS_HXX
