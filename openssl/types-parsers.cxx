// file      : openssl/types-parsers.cxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#include <openssl/types-parsers.hxx>

#include <openssl/options.hxx> // openssl::cli namespace

namespace openssl
{
  namespace cli
  {
    void parser<simulate_outcome>::
    parse (simulate_outcome& x, bool& xs, scanner& s)
    {
      xs = true;
      const char* o (s.next ());

      if (!s.more ())
        throw missing_value (o);

      const string v (s.next ());

      try
      {
        x = to_simulate_outcome (v);
      }
      catch (const invalid_argument&)
      {
        throw invalid_value (o, v);
      }
    }
  }
}
