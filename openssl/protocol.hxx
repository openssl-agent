// file      : openssl/protocol.hxx -*- C++ -*-
// license   : MIT; see accompanying LICENSE file

#ifndef OPENSSL_PROTOCOL_HXX
#define OPENSSL_PROTOCOL_HXX

#include <openssl/types.hxx>
#include <openssl/utility.hxx>

namespace openssl
{
  // Simple client/agent communication protocol.
  //

  // Return the file descriptor connected to the agent's Unix-domain socket.
  // Throw io_error on the underlying OS error.
  //
  auto_fd
  connect (const path& socket);

  class request
  {
  public:
    string       cmd;
    strings      args; // Shouldn't contain NULL characters.
    vector<char> input;

    // Create an empty request, normally before reading it from a stream.
    //
    request () = default;

    request (string c, strings a, vector<char> i)
        : cmd (move (c)), args (move (a)), input (move (i)) {}

    explicit
    request (string c): cmd (move (c)) {}
    request (string c, strings a): cmd (move (c)), args (move (a)) {}
    request (string c, vector<char> i): cmd (move (c)), input (move (i)) {}
  };

  class response
  {
  public:
    uint8_t      status;
    vector<char> output;
    string       error;

    response (uint8_t s, vector<char> o, string e)
        : status (s), output (move (o)), error (move (e)) {}

    // Success.
    //
    response (): status (0) {}

    explicit
    response (vector<char> o): status (0), output (move (o)) {}

    // Error.
    //
    explicit
    response (string e, uint8_t s = 1): status (s), error (move (e)) {}
  };

  // The stream's exception mask should have at least failbit and badbit set
  // (that is the default for fdstreams).
  //
  ostream& operator<< (ostream&, const request&);
  istream& operator>> (istream&,       request&);
  ostream& operator<< (ostream&, const response&);
  istream& operator>> (istream&,       response&);
}

#endif // OPENSSL_PROTOCOL_HXX
